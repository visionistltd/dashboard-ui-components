export class Notification {
    type: string;
    createdDate?: Date;
    message: string;
    imei: string;
}
 