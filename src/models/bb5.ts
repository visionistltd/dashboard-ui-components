import {Tag} from '../models/tag';

export class Bb5 extends Tag {
    lightSensor?: string;
    voiceSimMobileNumber?: string
    burstMode?: boolean;
    beaconMode?:boolean;
    cellLookUpMode?: boolean;
    voiceBoost?: boolean;
}