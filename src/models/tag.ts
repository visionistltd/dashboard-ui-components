
export class Tag {
    imei?: string;
    latitude?: number;
    longitude?: number;
    alarm?: boolean;
    alarmEnabled?: boolean;
    alarmState?: boolean;
    alarmModePending?: boolean;
    lastEventReceivedUtc?: Date;
    eventReceivedAsString?: string;
    simMobileNumber?: string;
    temperature?: string;
    voltage?: string;
}