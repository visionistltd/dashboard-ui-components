import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Notification } from '../../models/notification';
import { addScroll } from '../../assets/javascript/scroll.js';

@Component({
    selector: 'dashboard-feed',
    template: `
        <section widget class="widget large" *ngIf="notifications">
        <header>
            <h4>{{title}}</h4>
        </header>
        <div class="body" style="margin-top: 1em;">
            <div class="scroll">
                <div id="feed" class="feed">
                    <div class="wrapper">
                        <div class="vertical-line"></div>
                        <section class="feed-item" *ngFor="let notification of notifications">
                            <div class="icon pull-left">
                                <i class="fa fa-heartbeat" *ngIf="notification.type == 'healthCheck'"></i>
                                <i class="fa fa-battery-quarter" style="color: red;" *ngIf="notification.type == 'lowBattery'"></i>
                                <i class="fa fa-bell" *ngIf="notification.type == 'inAlarm'"></i>
                                <i class="fa fa-thermometer-three-quarters" style="color: red;" *ngIf="notification.type == 'highTemperature'"></i>
                                <i class="fa fa-location-arrow" *ngIf="notification.type == 'motionDetected'"></i>
                                <i class="fa fa-check" style="color: green;" *ngIf="notification.type == 'registered'"></i>
                            </div>
                            <div class="feed-item-body">
                                <div class="text" *ngIf="notification.message">
                                    <a href="{{notificationLink}}/{{notification.imei}}">{{notification.message}}</a>
                                </div>
                                <div class="time pull-left" *ngIf="notification.createdDate">
                                    {{notification.createdDate}}
                                </div>
                            </div>
                        </section>
                        <section class="feed-item" *ngIf="totalResults > notifications.length">
                            <div class="icon pull-left">
                                <i class="fa fa-arrow-down"></i>
                            </div>
                            <div class="feed-item-body">
                                <div class="text">
                                    <a class="more-results" (click)="loadMoreResults()">Load more...</a>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="slimScrollBar"></div>
                <div class="slimScrollRail"></div>
            </div>
        </div>
    </section>`,
    styles: [`
        .scroll {
            position: relative; 
            overflow: hidden; 
            width: auto; 
            height: 230px;
        }
        
        .feed {
            overflow: hidden; 
            width: auto; 
            padding: 0;
            height: 230px;
        }
        
        .more-results {
            color: #fff;
            &:hover {
                background: rgba(0, 0, 0, 0.24);
                cursor: pointer;
            }
        }`
    ]
  })
  export class FeedComponent {

    @Input()
    notifications: Notification[];

    @Input()
    title: string;

    //Used to hide / display the more options
    @Input()
    totalResults: number;

    //Event trigger if user selects more results option
    @Output()
    loadMore = new EventEmitter<boolean>();

    @Input()
    notificationLink: string;

    loadMoreResults(){
      this.loadMore.emit(true);
    }

    ngAfterViewInit(){
        addScroll();
      }
  }