
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Tag } from '../../models/tag';

@Component({
    moduleId: module.id,
    selector: 'tag-summary',
    template: `
    <div class="col-md-12">
    <h3 class="page-title" *ngIf="showTitle">{{title}} <span class="fw-semi-bold"> {{titleBold}}</span></h3>
</div>
<div class="col-md-12" style="margin-bottom: 4em;">
    <div *ngIf="totalResults > 0 && maxPage !== 1">
        <div class="btn-group">
            <div class="dropdown">
                <button class="btn btn-primary" data-toggle="dropdown" type="button">Show {{currentPageSize}} per page<i class="fa fa-chevron-down" style="padding-left: 5px;"></i></button>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" on-click="updateTotalResultsToDisplay(10)">10</a></li>
                    <li><a class="dropdown-item" on-click="updateTotalResultsToDisplay(20)">20</a></li>
                    <li><a class="dropdown-item" on-click="updateTotalResultsToDisplay(30)">30</a></li>
                    <li><a class="dropdown-item" on-click="updateTotalResultsToDisplay(50)">50</a></li>
                </ul>
            </div>
        </div> 
        <div class="screen-layout-options">
            <i class="fa fa-th" [ngClass]="{'active' : gridView}" (click)="tagView('grid')"></i>
            <i class="fa fa-bars" [ngClass]="{'active' : tableView}" (click)="tagView('table')"></i>
        </div>
    </div>
    <div class="pagination-group" *ngIf="pagination && pagination.pages && pagination.totalPages > 1"> 
        <ul class="pagination pagination-sm pagination-search-result">
            <li (click)="search(currentPage - 1)" *ngIf="currentPage !== 1"><a class="active">Previous</a></li>
            <li *ngFor="let item of pagination.pages; let i = index; last as isLast;" (click)="search(i + 1)">
                <a class="active" [ngClass]="{'current-page' : currentPage == item}">{{item}}</a>
            </li>
            <li (click)="search(currentPage + 1)" *ngIf="currentPage !== pagination.totalPages"><a class="active">Next</a></li>
        </ul>
    </div>
</div>
 
<div class="col-md-12">
    <div class="row" *ngIf="tableView">
        <div class="col-md-12">
            <tag-table-summary [tags]="tags" [tagLink]="tagLink" [title]="Active"></tag-table-summary>
        </div>
    </div>
    <div class="row" *ngIf="gridView">
        <div class="col-md-6" style="margin-bottom: 2em;" *ngFor="let tag of tags" >
            <tag-grid-view [tag]="tag" [tagLink]="tagLink"></tag-grid-view>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="pagination-group" *ngIf="pagination && pagination.pages && pagination.totalPages > 1"> 
        <ul class="pagination pagination-sm pagination-search-result">
            <li (click)="search(currentPage - 1)" *ngIf="currentPage !== 1"><a class="active">Previous</a></li>
            <li *ngFor="let item of pagination.pages; let i = index; last as isLast;" (click)="search(i + 1)">
                <a class="active" [ngClass]="{'current-page' : currentPage == item}">{{item}}</a>
            </li>
            <li (click)="search(currentPage + 1)" *ngIf="currentPage !== pagination.totalPages"><a class="active">Next</a></li>
        </ul>
    </div>
</div>

    `,
    styles: [`
    .screen-layout-options{
      float: left;
      color: #fff;
      background-color: rgba(0,0,0,.31);
      padding: 3px;
      border-radius: 10px;
      margin-bottom: 1rem;
      margin-left: 10px;
    }
    
    .screen-layout-options i {
      font-size: 2em;
      margin: 2px;
      &:hover {
        cursor: pointer;
        color: rgba(0,0,0,.31);
      }
    }
    
    .active {
      color: rgba(0,0,0,.31);
    }
  
    .pagination { 
      display: inline-block;
      padding-left: 0;
      border-radius: 3px;
      font-weight: 400;
      color: white;
  }
  
  
  .pagination-search-results li a{
    background-color: #5D6B7A;
  }
  
  .pagination-search-result li a {
    color: #f8f8f8;
    border-radius: 3px;
    margin: 0 2px;
  
    &:hover {
      background: rgba(0, 0, 0, 0.24);
      cursor: pointer;
    }
  }
  
  .current-page {
    background: rgba(0, 0, 0, 0.24);
  }
  
  .pagination-group{
    float: right;
  }
  
  .pagination-sm li a {
    padding: 4px 10px;
    font-size: 12px;
    line-height: 1.5;
  }
  
  .totalResults {
    font-weight: 400;
    font-size: 1rem;
  }
  
  .dropdown-menu{
    background-color: #fff;
  }
  
  .dropdown li a {
    color: #666;
    font-size: 1rem;
    &:hover {
      color: #fff;
      font-weight: 300;
    }
  }
  
  .btn-group {
    float: left;
  }
  
  .dropdown-menu li a {
    color: #666;
    font-size: 1rem;
    &:hover {
      color: #fff;
      font-weight: 300;
    }
  }
  
  .dropdown-item {
    background-color: #fff;
    color: #666;
    font-weight: 300;
    font-size: 1rem;
    text-align: center;
    &:hover {
      background-color: #8ec5fd;
      color: #fff;
    }
  
    .span .totalResults {
      font-size: 20px;
      font-weight: 300;
      color: #fff;
    }
  }`]
  })
export class TagSummaryComponent
{
    @Input()
    tags: Tag[];

    @Input()
    title: string;

    @Input()
    titleBold: string;

    @Input()
    showTitle: boolean = false;

    @Input()
    tableView: boolean = true;

    @Input()
    gridView: boolean = false;

    @Input()
    pagination = {}

    @Input()
    totalResults: number;

    @Input()
    showPagination: boolean = false;

    @Input()
    currentPage: number;

    @Input()
    currentPageSize: number;

    @Output()
    selectedPage = new EventEmitter<number>();

    @Output()
    pageSize = new EventEmitter<number>();

    @Input()
    tagLink: string;

    tagView(type: string){
      if(type == 'table'){
        this.tableView = true;
        this.gridView = false;
      }else{
        this.gridView = true;
        this.tableView = false;
      }
    }

    search(pageNumber: number){
      this.selectedPage.emit(pageNumber);
    }

    updateTotalResultsToDisplay(pageSize: number){
      this.pageSize.emit(pageSize);
      //Forces to search again
      this.selectedPage.emit(1);
    }
}