import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagTableSummaryComponent } from './components/tag-table-summary-component/tag-table-summary.component';
import { TagGridSummaryComponent } from './components/tag-grid-summary-component/tag-grid-summary.component';
import { TagSummaryComponent } from './components/tag-summary-component/tag-summary.component';
import {RouterModule} from '@angular/router';
import 'jquery-slimscroll/jquery.slimscroll.min.js'

@NgModule({
  declarations: [
    TagTableSummaryComponent, 
    TagGridSummaryComponent, 
    TagSummaryComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    TagTableSummaryComponent, 
    TagGridSummaryComponent, 
    TagSummaryComponent, 
  ]
})
export class DashboardModule { }
