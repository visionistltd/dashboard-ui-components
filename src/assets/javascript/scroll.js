$(document).ready(function(){
    addScroll();
  });

export function addScroll(){
    $("#feed").slimscroll({
        height: "auto",
        size: "5px",
        alwaysVisible: !0,
        railVisible: !0
    }); 
  }