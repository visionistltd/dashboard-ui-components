# Smarter Technologies Dashboard UI Components

This repository contains resuable modules, components and models that can be used to create a Smarter Technologies dashboard.

## Modules & Components

* DashboardModule - Includes the below components
* TagSummaryComponent - Display the tag in a table or grid view and includes pagination
* TagGridSummaryComponent - Display the tag in a grid view
* TagTableSummaryComponent - Display the tag in a table view
* FeedComponent - Display tag events in a notification feed

## Installation

```	npm install dashboard-ui-components@{version}```

## Set-up

Import the dashboard module or required components

```	import { DashboardModule } from 'dashboard-ui-components';```


Add DashboardModule to your modules declarations:


```typescript
@NgModule({
  imports: [
    ...,
    DashboardModule
  ],
  ...
})
class YourModule { ... }
```

### TagSummaryComponent Example

```
<tag-summary 
    [bb5s]="searchResults.results" 
    [pagination]="pagination" 
    [currentPage]="currentPage" 
    [totalResults]="totalResults" 
    [showPagination]="true" 
    (selectedPage)="search($event, pageSize)" 
    (pageSize)="setPageSize($event)" 
    [currentPageSize]="pageSize" 
    [showTitle]="true" 
    [title]="'Matching'" 
    [titleBold]="' Results'"
    [tagLink]="'/app/area'"
  </tag-summary>
    
```

### Configuration:


#### Input

* **`bb5s`** [`Tag[]`] - **required** Bb5's to display.
* **`pagination`** [`number`] - Pagination object.
* **`currentPage`** [`number`] The current active page number.
* **`totalResults`** [`number`] Total results, NOT the current page results.
* **`showPagination`** [`boolean`] - Show pagination or not.
* **`currentPageSize`** [`number`] - The number of items to display per page.
* **`showTitle`** [`boolean`] - Whether to display a title or not.
* **`title`** [`string`] - Title to display at the top of the page, not in bold.
* **`titleBold`** [`string`] - Title to display in bold.
* **`tagLink `** [`string`] - Link to redirect the user to when they select the imei number, please note that imei is appended to the end of the link provided.

#### Output

* **`selectedPage`** [`Event`] - Selected page event occurs when user requests to see a different page of results.
* **`pageSize`** [`Event`] - Page size event occurs when a user changes the number of results to see per screen. 

### TagTableSummaryComponent Example

Provide a list of bb5 objects.

```
<tag-table-summary [tag]="tags" [title]="Active" [tagLink]="'/app/device'"></tag-table-summary>
```


### TagGridSummaryComponent Example

Provide a single bb5 object

```
  <tag-grid-view [tag]="tag" [tagLink]="'/app/device'"></tag-grid-view>
```


### FeedComponent

```
      <dashboard-feed 
        [title]="'Recent events'" 
        [notifications]="notifications" 
        (loadMore)="showMoreFeedResults()" 
        [totalResults]="totalNotifications",
        [notificationLink]="'/app/area'"
      </dashboard-feed>
      
```


### Configuration:


#### Input

* **`notifications`** [`Notification[]`] - **required** Notifications to display.
* **`totalResults`** [`number`] Total notifications.
* **`title`** [`string`] - Title to display at the top of the feed.
* **`notificationLink `** [`string`] - Link to redirect the user to when they select the imei number, please note that imei is appended to the end of the link provided.


#### Output

* **`loadMore`** [`Event`] - Load more event occurs when user requests to load more notifications.
